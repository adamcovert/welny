$(document).ready(function() {

  // Owl Carousel Slider
  $('#masters-slider').owlCarousel({
    margin: 15,
    mouseDrag: false,
    navText: ['<svg width="30px" height="40px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#e0862f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="30px" height="40px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#e0862f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
        dots: true
      },
      468: {
        items: 2,
        dots: true
      },
      768: {
        items: 3,
        dots: false,
        nav: true
      },
      992: {
        items: 4,
        dots: false,
        nav: true
      },
      1200: {
        items: 4,
        dots: false,
        nav: true
      }
    }
  });

  // Turn on Modal
  modals.init();

});